package teamcactus.welcome.project.model;

/**
 *
 * @author Quentin
 */
public class BroModel {
    private String name;
    private String login;
    private Integer lvl;
    private String hashMdp;

    public BroModel(String name, String login, Integer lvl, String hashMdp) {
        this.name = name;
        this.login = login;
        this.lvl = lvl;
        this.hashMdp = hashMdp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

    public String getHashMdp() {
        return hashMdp;
    }

    public void setHashMdp(String hashMdp) {
        this.hashMdp = hashMdp;
    }
}
